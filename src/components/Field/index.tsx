import styledTS from 'styled-components-ts'

import Flex from '../Flex'
import { Sizes } from '../../styles'

interface FieldProps {
  marginless?: boolean
}

const Field = styledTS<FieldProps>(Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'flex-start'
}))`
  width: 100%;
  margin-bottom: ${props => props.marginless ? 0 : Sizes.size4};
`

export default Field
