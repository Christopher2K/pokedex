import * as React from 'react'
import styled from 'styled-components'

import Grid from '../Grid'
import Button from '../Button'
import Message from '../Message'
import Loading from '../Loading'
import PokemonItem from '../PokemonItem'
import { Sizes } from '../../styles'

interface PokemonList {
  pokemonList: Model.Pokemons
  maxItems: number
  loading: boolean
  onNextClick (): void
  onPokemonClick (pokemon: Model.Pokemon): void
}

const MessageContainer = styled(({ className, children }: any) => (
  <Grid.Container className='is-centered'>
    <Grid.Column columnSize='12' className='has-text-centered'>
      {children}
    </Grid.Column>
  </Grid.Container>
))`
  margin: ${Sizes.size6} 0;
`

const CenteredButton = Button.extend`
  margin: auto;
`

const ListLoading = () => (
  <Grid.Column columnSize='12' className='has-text-centered'>
    <Loading iconSize='large' />
  </Grid.Column>

)

const PokemonList: React.SFC<PokemonList> = ({
  pokemonList,
  maxItems,
  onNextClick,
  loading,
  onPokemonClick
}) => (
  <Grid.Container className='is-multiline'>
    {loading ? (
      <ListLoading />
    ) : (
      <>
        {pokemonList.map((pokemon: Model.Pokemon) => (
          <Grid.Column key={pokemon.id} columnSize='6' className='has-text-centered'>
            <PokemonItem
              pokemon={pokemon}
              onPokemonClick={onPokemonClick}
            />
          </Grid.Column>
        ))}
        <MessageContainer>
          {(pokemonList.length < maxItems) && (
            <CenteredButton
              onClick={onNextClick}
              btnSize='large'
            >
              See more pokemons
            </CenteredButton>
          )}
          {(pokemonList.length === maxItems && maxItems !== 0) && (
            <Message kind='info'>
              No more pokemons to show
            </Message>
          )}
          {(maxItems === 0) && (
            <Message kind='warning'>
              No pokemons found
            </Message>
          )}
        </MessageContainer>
      </>
    )}
  </Grid.Container>
)

export default PokemonList
