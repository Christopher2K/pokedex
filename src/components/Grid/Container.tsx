import styled from 'styled-components'
import styledTS from 'styled-components-ts'

export interface ContainerProps {}

const Container = styledTS<ContainerProps>(styled.div.attrs({
  className: 'columns'
}))``

export default Container
