import styled from 'styled-components'
import styledTS from 'styled-components-ts'

export interface ColumnProps {
  columnSize: '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | '11' | '12'
}

const Column = styledTS<ColumnProps>(styled.div.attrs({
  className: (props: ColumnProps) => `column is-${props.columnSize}`
}))``

Column.defaultProps = {
  columnSize: '12'
}

export default Column
