import * as React from 'react'
import { NavLink } from 'react-router-dom'

interface NavbarItemProps {
  to: string
  children: string
  onClick? (): void
}

const NavbarItem: React.SFC<NavbarItemProps> = ({
  to,
  children,
  onClick
}) => (
  <NavLink
    onClick={onClick}
    exact
    to={to}
    className='navbar-item'
    activeClassName='is-active'
  >
    {children}
  </NavLink>
)

NavbarItem.defaultProps = {
  onClick: () => null
}

export default NavbarItem
