import * as React from 'react'
import Moment from 'moment'

import Flex from '../Flex'
import Title from '../Title'
import { Sizes } from '../../styles'

const Root = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'flex-start'
})`
  width: 100%
  margin-bottom: ${Sizes.size5};
`

interface TweetProps {
  tweet: Model.Tweet
}

const Tweet: React.SFC<TweetProps> = ({
  tweet
}) => (
  <Root>
    <Title.H5 marginless>
      By <a target='_blank' href={`https://twitter.com/${tweet.username}`}>{tweet.user}</a> at {Moment(tweet.created_at).format('LLLL')}
    </Title.H5>
    <p>
      "{tweet.text}"
    </p>
  </Root>
)

export default Tweet
