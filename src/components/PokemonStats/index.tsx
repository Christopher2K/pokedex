import * as React from 'react'
import styled from 'styled-components'
import Pokedex from 'pokedex-promise-v2'

import Flex from '../Flex'
import Stat from '../Stat'
import Title from '../Title'
import { Sizes } from '../../styles'

const Root = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'flex-start'
})`
  width: 100%;
  margin-bottom: ${Sizes.size5};
`

const StatsContainer = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'flex-start'
})`
  width: 100%;
  margin: ${Sizes.size5} 0;
`

const RadioLabel = styled.label.attrs({
  className: 'radio'
})`
  font-size: ${Sizes.typo4};
  margin-left: ${Sizes.typo6};
  font-weight: 300;
`

const Radio = styled.input`
  margin-right: ${Sizes.typo6};
`

interface PokemonStatsProps {
  pokemon: Model.Pokemon
  details: Pokedex.Pokemon
  typeDetails: Model.TypeDetails[]
  currentType: Model.TypeDetails
  onTypeClicked (event: React.MouseEvent<HTMLInputElement>): void
}

const PokemonStats: React.SFC<PokemonStatsProps> = ({
  pokemon,
  details,
  typeDetails,
  currentType,
  onTypeClicked
}) => (
  <Root>
    <Title.H3>{pokemon.name}'s base stats</Title.H3>
    <p>You can compare his base stats with pokemons that have the same type.</p>
    <StatsContainer>
      {details.stats.map((stat: Pokedex.PokemonStat) => (
        <Stat
          key={stat.stat.name}
          stat={stat.stat.name as Model.PokemonBaseStat}
          value={stat.base_stat}
          min={stat.base_stat < currentType[stat.stat.name].min ? stat.base_stat : currentType[stat.stat.name].min}
          max={stat.base_stat > currentType[stat.stat.name].max ? stat.base_stat : currentType[stat.stat.name].max}
        />
      ))}
    </StatsContainer>
    <Title.H4>Comparison based on:</Title.H4>
    {typeDetails.map((type: Model.TypeDetails) => (
      <div key={type.name}>
        <RadioLabel className='radio'>
          <Radio
            type='radio'
            name={type.name}
            onClick={onTypeClicked}
            onChange={() => null}
            checked={currentType.name === type.name}
          />
          {type.name}
        </RadioLabel>
      </div>
    ))}
  </Root>
)

export default PokemonStats
