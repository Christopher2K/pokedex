import styled from 'styled-components'
import styledTS from 'styled-components-ts'

export interface ContainerProps {
  inline?: boolean
  reverse?: boolean
  direction?: 'column' | 'row'
  justify?: 'flex-start' | 'flex-end' | 'center' | 'space-around' | 'space-between'
  align?: 'flex-start' | 'flex-end' | 'center' | 'space-around' | 'space-between' | 'stretch'
}

const Container = styledTS<ContainerProps>(styled.div)`
  display: ${props => props.inline ? 'inline-flex' : 'flex'};
  flex-direction: ${props => props.reverse ? `${props.direction}-reverse` : props.direction};
  justify-content: ${props => props.justify};
  align-items: ${props => props.align};
`

Container.defaultProps = {
  direction: 'row',
  justify: 'flex-start',
  align: 'flex-start'
}

export default Container
