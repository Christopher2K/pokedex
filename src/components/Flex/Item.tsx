import styled from 'styled-components'
import styledTS from 'styled-components-ts'

export interface ItemProps {
  grow?: number | 'auto'
  shrink?: number | 'auto'
  align?: 'auto' | 'flex-start' | 'flex-end' | 'center' | 'space-around' | 'space-between'
}

const Item = styledTS<ItemProps>(styled.div)`
  flex-grow: ${props => props.grow || 'auto'};
  flex-shrink: ${props => props.shrink || 'auto'};
  align-self: ${props => props.align || 'auto'};
`

Item.defaultProps = {
  grow: 'auto',
  shrink: 'auto',
  align: 'auto'
}

export default Item
