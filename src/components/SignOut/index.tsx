import * as React from 'react'
import { Redirect } from 'react-router-dom'
import { lifecycle } from 'recompose'

interface SignOutProps {
  history: History
  signOut (): void
}

namespace withLifecycle {
  export interface DispatchProps {
    signOut (): void
  }

  export const hoc = lifecycle<SignOutProps, {}, {}>({
    componentDidMount () {
      this.props.signOut()
    }
  })
}

const SignOut: React.SFC = () => (
  <Redirect to='/app' />
)

export default withLifecycle.hoc(SignOut)
