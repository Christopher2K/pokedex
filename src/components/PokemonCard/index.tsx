import * as React from 'react'
import styled from 'styled-components'
import Pokedex from 'pokedex-promise-v2'

import Title from '../Title'
import Flex from '../Flex'
import Data from './Data'
// import Vote from './Vote'
import { Colors, Values, Sizes } from '../../styles'

const Root = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'center'
})`
  width: 100%;
  border: 1px solid ${Colors.blazePurple};
  border-radius: ${Values.radius};
  box-shadow: ${Values.shadow};
  padding: ${Sizes.size5};
`

const PokemonImage = styled.img`
  height: 170px;
  width: auto;
  max-width: 100%;
  margin: ${Sizes.size5} 0;
`

export interface PokemonCardProps {
  pokemon: Model.Pokemon
  details: Pokedex.Pokemon
  voted?: boolean
  onThumbUpClick? (): void
  onThumbDownClick? (): void
}

const PokemonCard: React.SFC<PokemonCardProps> = ({
  pokemon,
  details,
  children,
  voted,
  onThumbUpClick,
  onThumbDownClick
}) => (
  <Root>
    <Title.H3 marginless>
      {pokemon.name}
    </Title.H3>
    <PokemonImage
      src={pokemon.artwork}
      alt={pokemon.name}
    />
    {/* <Vote
      voted={voted}
      onThumbUpClick={onThumbUpClick}
      onThumbDownClick={onThumbDownClick}
    /> */}
    <Data
      pokemon={pokemon}
      details={details}
    />
    {children}
  </Root>
)

PokemonCard.defaultProps = {
  onThumbUpClick: () => console.log('+1 on this pokemon'),
  onThumbDownClick: () => console.log('-1 on this pokemon')
}

export default PokemonCard
