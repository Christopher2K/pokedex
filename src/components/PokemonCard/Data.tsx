import * as React from 'react'
import styled from 'styled-components'
import styledTS from 'styled-components-ts'
import Pokedex from 'pokedex-promise-v2'

import Flex from '../Flex'
import { Colors, Sizes, Values } from '../../styles'
import { TypeColors } from '../../lib'

const Root = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'flex-start'
})`
  width: 100%
`

const DataRoot = Flex.Container.extend.attrs({
  direction: 'row',
  justify: 'flex-start',
  align: 'center'
})`
  flex-wrap: wrap;
  width: 100%;
`

const DataValue = styled.span`
  color: ${Colors.darkGrey};
`

const DataLabel = styled.span`
  font-size: ${Sizes.typo5};
  color: ${Colors.black};
`

const DataType = styledTS<{type: string}>(styled.span)`
  font-size: ${Sizes.typo7};
  color: ${Colors.white};
  margin-right: ${Sizes.size7};
  padding: 5px;
  background-color: ${props => TypeColors[props.type.toLowerCase()] || Colors.black};
  border-radius: ${Values.radius};
`

export interface DataProps {
  pokemon: Model.Pokemon
  details: Pokedex.Pokemon
}

const Data: React.SFC<DataProps> = ({
  pokemon,
  details
}) => (
  <Root>
    <DataRoot>
      <DataLabel>Number: &nbsp;</DataLabel>
      <DataValue>#{pokemon.id}</DataValue>
    </DataRoot>
    <DataRoot>
      <DataLabel>Height: &nbsp;</DataLabel>
      <DataValue>{details.height}</DataValue>
    </DataRoot>
    <DataRoot>
      <DataLabel>Weight: &nbsp;</DataLabel>
      <DataValue>{details.weight}</DataValue>
    </DataRoot>
    <DataRoot>
      <DataLabel>Types: &nbsp;</DataLabel>
      {pokemon.types.map(type => (
        <DataType key={type} type={type}>{type}</DataType>
      ))}
    </DataRoot>
  </Root>
)

export default Data
