import * as React from 'react'
import styled from 'styled-components'

import Title from '../Title'
import Flex from '../Flex'
import { Sizes } from '../../styles'
import thumbUp from '../../assets/icons/thumb-up.svg'
import thumbDown from '../../assets/icons/thumb-down.svg'

const Root = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'flex-start'
})`
  width: 100%;
`

const ThumbsContainer = Flex.Container.extend.attrs({
  direction: 'row',
  justify: 'space-around',
  align: 'center'
})`
  width: 100%;
  margin: ${Sizes.size6} 0;
`

const ThumbButton = styled.button`
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border: none;
  cursor: pointer;
  background-color: transparent;
`

const Thumb = styled.img`
  height: 30px;
  widht: auto;
`

interface VoteProps {
  voted?: boolean
  onThumbUpClick? (): void
  onThumbDownClick? (): void
}

const Vote: React.SFC<VoteProps> = ({
  voted,
  onThumbUpClick,
  onThumbDownClick
}) => (
  <Root>
    {voted ? (
      <>
        {/* TODO: DATAVIZ Likes & dislikes*/}
        <p>Dataviz on likes / dislikes here</p>
      </>
    ) : (
      <>
        <Title.H5 marginless >Love this pokemon ?</Title.H5>
        <ThumbsContainer>
          <ThumbButton
            onClick={onThumbUpClick}
            type='button'>
            <Thumb
              src={thumbUp}
              alt='+1'
            />
          </ThumbButton>
          <ThumbButton
            onClick={onThumbDownClick}
            type='button'>
            <Thumb
              src={thumbDown}
              alt='-1'
            />
          </ThumbButton>
        </ThumbsContainer>
      </>
    )}
  </Root>
)

export default Vote
