import * as React from 'react'
import styled from 'styled-components'

import Input from '../Input'
import Field from '../Field'
import Button from '../Button'
import Message from '../Message'
import { Sizes } from '../../styles'
import { fieldsAreClean } from '../../lib'

const Root = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
`

const OptionContainer = styled.div`
  width: 100%;
  text-align: left;
  margin-top: ${Sizes.size7} !important;
`

export interface AuthFormProps {
  optionComponent?: React.ReactNode
  email: Type.Field
  password: Type.Field
  loading?: boolean
  error?: string
  onSubmit (evt: React.FormEvent<HTMLFormElement>): void
  onChange (evt: React.ChangeEvent<HTMLInputElement>): void
}

const AuthFormProps: React.SFC<AuthFormProps> = ({
  optionComponent,
  error,
  onSubmit,
  onChange,
  email,
  password,
  loading
}) => (
  <Root onSubmit={onSubmit}>
    <Field>
      <Input
        name='email'
        type='email'
        value={email.value}
        onChange={onChange}
        placeholder='Email'
        required
      />
    </Field>
    <Field>
      <Input
        name='password'
        type='password'
        value={password.value}
        onChange={onChange}
        placeholder='My-Secret-Password'
        required
      />
      {optionComponent && (
        <OptionContainer>
          {optionComponent}
        </OptionContainer>
      )}
    </Field>
    {error && (
      <Message kind='danger'>{error}</Message>
    )}
    <Field>
      <Button
        type='submit'
        btnSize='large'
        disabled={loading || !fieldsAreClean([email, password])}
        loading={loading}
      >
        Go !
      </Button>
    </Field>
  </Root>
)

export default AuthFormProps
