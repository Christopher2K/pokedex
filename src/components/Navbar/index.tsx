import * as React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import Container from '../Container'
import zenguball from '../../assets/icons/zenguball.svg'

const Logo = styled.img`
  content: url("${zenguball}");
  height: 3rem;
  width: auto;
`

interface NavbarProps {
  isOpen: boolean
  children: React.ReactNode
  onBurgerClick (): void
}

const Navbar: React.SFC<NavbarProps> = ({
  isOpen,
  onBurgerClick,
  children
}) => (
  <nav className='navbar is-fixed-top' role='navigation' aria-label='main navigation'>
    <Container>
      <div className='navbar-brand'>
        <Link className='navbar-item' to='/app'>
          <Logo alt='Zengularity Ball' />
        </Link>

        <a
          role='button'
          onClick={onBurgerClick}
          className={`navbar-burger${isOpen ? ' is-active' : ''}`}
        >
          <span></span>
          <span></span>
          <span></span>
        </a>
      </div>
      <div className={`navbar-menu navbar-end${isOpen ? ' is-active' : ''}`}>
        {children}
      </div>
    </Container>
  </nav>
)

export default Navbar
