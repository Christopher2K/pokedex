
import Flex from '../Flex'

const AppLayout = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'flex-start'
})`
  position: relative;
  width: 100%;
  overflow-y: auto;
  -webkit-overflow-scrolling: touch;
  padding-top: 5rem;
  height: 100vh;
`

export default AppLayout
