import styled, { css } from 'styled-components'
import styledTS from 'styled-components-ts'

import { Colors, Sizes } from '../../styles'

export interface TitleProps {
  marginless?: boolean
  align?: 'left' | 'right' | 'center'
}

const TitleDefaultProps: TitleProps = {
  align: 'center'
}

function commonCss (props: TitleProps) {
  return css`
    text-align: ${(props: TitleProps) => props.align};
    margin-top: 0;
    color: ${Colors.darkGrey};
    ${props.marginless && 'margin: 0 !important;'}
  `
}

const H1 = styledTS<TitleProps>(styled.h1)`
  font-size: ${Sizes.typo1};
  font-weight: 700;
  margin-bottom: ${Sizes.size4};
  ${commonCss}
`
H1.defaultProps = { ...TitleDefaultProps }

const H2 = styledTS<TitleProps>(styled.h2)`
  font-size: ${Sizes.typo2};
  font-weight: 700;
  margin-bottom: ${Sizes.size4};
  ${commonCss}
`
H2.defaultProps = { ...TitleDefaultProps }

const H3 = styledTS<TitleProps>(styled.h3)`
  font-size: ${Sizes.typo3};
  font-weight: 700;
  margin-bottom: ${Sizes.size5};
  ${commonCss}
`
H3.defaultProps = { ...TitleDefaultProps }

const H4 = styledTS<TitleProps>(styled.h4)`
  font-size: ${Sizes.typo4};
  font-weight: 600;
  margin-bottom: ${Sizes.size5};
  ${commonCss}
`
H4.defaultProps = { ...TitleDefaultProps }

const H5 = styledTS<TitleProps>(styled.h5)`
  font-size: ${Sizes.typo5};
  font-weight: 500;
  margin-bottom: ${Sizes.size6};
  ${commonCss}
`
H5.defaultProps = { ...TitleDefaultProps }

const H6 = styledTS<TitleProps>(styled.h6)`
  font-size: ${Sizes.typo6};
  font-weight: 500;
  margin-bottom: ${Sizes.size6};
  ${commonCss}
`
H6.defaultProps = { ...TitleDefaultProps }

export default {
  H1,
  H2,
  H3,
  H4,
  H5,
  H6
}
