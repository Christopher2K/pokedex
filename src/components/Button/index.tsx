import styled, { css } from 'styled-components'
import styledTS from 'styled-components-ts'

import { Values, Colors, Sizes } from '../../styles'

export interface ButtonProps {
  loading?: boolean
  shadow?: boolean
  btnSize?: 'small' | 'normal' | 'large'
}

const Button = styledTS<ButtonProps>(styled.button)`
  position: relative;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: center;
  justify-content: center;

  border: none;
  border-radius: ${Values.radius};
  background-color: ${Colors.secondary};
  color: ${Colors.white};
  ${props => props.shadow && css`
    box-shadow: ${Values.shadow};
  `}
  font-size: ${props => {
    switch (props.btnSize) {
      case 'small': return Sizes.typo7
      case 'normal': return Sizes.typo6
      case 'large': return Sizes.typo4
      default: return Sizes.typo6
    }
  }};
  padding: ${props => {
    switch (props.btnSize) {
      case 'small': return Sizes.size7
      case 'normal': return Sizes.size6
      case 'large': return Sizes.size5
      default: return Sizes.size6
    }
  }};
  cursor: pointer;

  &:disabled {
    cursor: not-allowed;
    box-shadow: none;
    background-color: ${Colors.grey};
  }

  ${props => props.loading && css`
    color: transparent !important;
    &::after {
      animation: spin 500ms infinite linear;
      border: 2px solid ${Colors.white};
      border-radius: 99999px;
      border-right-color: transparent;
      border-top-color: transparent;
      content: '';
      display: block;
      height: 1em;
      width: 1em;

      position: absolute;
      left: calc(50% - (1em / 2));
      top: calc(50% - (1em / 2));
      position: absolute !important;
    }
  `}
`

Button.defaultProps = {
  btnSize: 'normal',
  shadow: true
}

export default Button
