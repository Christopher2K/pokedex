import Flex from '../Flex'
import Logo from './Logo'
import { Colors, Breakpoints } from '../../styles'

const RightPart = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'center',
  align: 'center'
})`
  height: 100%;
  flex-grow: 1;
  flex-shrink: 1;
  background-color: ${Colors.primary};

  ${Logo} {
    display: none
  }

  ${Breakpoints.belowDesktop} {
    background-color: ${Colors.white};
    ${Logo} {
      display: block
      max-width: 50%;
      margin-bottom: 15%;
    }
  }
`

export default RightPart
