import * as React from 'react'

import Flex from '../Flex'
import Logo from './Logo'
import LeftPart from './LeftPart'
import RightPart from './RightPart'

const Root = Flex.Container.extend.attrs({
  direction: 'row'
})`
  height: 100%
`

const ChildrenContainer = Flex.Container.extend.attrs({
  direction: 'column'
})`
  width: 100%
`

export interface UserAccountLayoutProps {
  children: any
}

const UserAccountLayout: React.SFC<UserAccountLayoutProps> = ({
  children
}) => (
  <Root>
    <LeftPart>
      <Logo />
    </LeftPart>
    <RightPart>
      <Logo />
      <ChildrenContainer>
        {children}
      </ChildrenContainer>
    </RightPart>
  </Root>
)

export default UserAccountLayout
