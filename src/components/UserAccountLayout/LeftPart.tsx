import Flex from '../Flex'
import { Sizes, Colors, Breakpoints } from '../../styles'

const LeftPart = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'center',
  align: 'center'
})`
  height: 100%;
  width: 35%;
  flex-shrink: 0;
  background-color: ${Colors.white};
  padding: ${Sizes.size6};

  ${Breakpoints.belowDesktop} {
    display: none;
  }
`

export default LeftPart
