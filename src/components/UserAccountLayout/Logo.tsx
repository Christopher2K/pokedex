import styled from 'styled-components'

import logo from '../../assets/logo.svg'

const Logo = styled.img`
  content: url('${logo}');
  width: 80%;
  height: auto;
`

export default Logo
