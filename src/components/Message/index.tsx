import * as React from 'react'
import styled from 'styled-components'
import styledTS from 'styled-components-ts'

export interface MessageProps {
  kind?: 'success' | 'error' | 'warning' | 'danger' | 'info'
}

const Root = styledTS<MessageProps>(styled.article.attrs({
  className: (props: MessageProps) => `message is-${props.kind}`
}))``

const Message: React.SFC<MessageProps> = (props) => (
  <Root kind={props.kind}>
    <div className='message-body'>{props.children}</div>
  </Root>
)

export default Message
