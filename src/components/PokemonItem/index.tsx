import * as React from 'react'
import styled from 'styled-components'

import Icon from './Icon'
import Title from '../Title'
import Flex from '../Flex'
import { Sizes, Colors } from '../../styles'
import { TypeIcons } from '../../lib'
import loading from '../../assets/icons/loading-image.svg'

const Root = Flex.Container.extend.attrs({
  direction: 'row',
  justify: 'flex-start',
  align: 'stretch'
})`
  width: 100%
  border: 2px solid ${Colors.primary};
  padding: ${Sizes.size6};
  border-radius: 5px;
  cursor: pointer;

  &:hover {
    border-color: ${Colors.secondary};
    background-color: ${Colors.lightGrey};
  }
`

const IconContainer = Flex.Container.extend.attrs({
  direction: 'row',
  justify: 'center',
  align: 'center'
})`
  position: relative
  height: 64px;
  width: auto;
  margin-right: ${Sizes.size6};
`

const InfoContainer = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'space-between',
  align: 'flex-start'
})``

const TypesContainer = Flex.Container.extend.attrs({
  direction: 'row',
  justify: 'center',
  align: 'center'
})``

const TypeIcon = styled.img`
  height: 30px;
  width: auto;
  margin-right: ${Sizes.size6};
`

export interface PokemonItemProps {
  pokemon: Model.Pokemon
  onPokemonClick (pokemon: Model.Pokemon): void
}

export interface PokemonItemState {
  iconLoading: boolean
}

class PokemonItem extends React.Component<PokemonItemProps, PokemonItemState> {
  public state: PokemonItemState = {
    iconLoading: true
  }

  public render (): React.ReactNode {
    const { iconLoading } = this.state
    const { pokemon } = this.props
    return (
      <Root onClick={this.pokemonClick}>
        <IconContainer>
          <Icon
            src={pokemon.artwork}
            alt={pokemon.name}
            showed={!iconLoading}
            onLoad={this.iconLoading}
          />
          {iconLoading && (
            <Icon
              src={loading}
              alt='Chargement...'
              showed
            />
          )}
        </IconContainer>
        <InfoContainer>
          <Title.H4 marginless>{pokemon.name}</Title.H4>
          <TypesContainer>
            {pokemon.types.map(type => (
              <TypeIcon
                key={type}
                src={TypeIcons[type.toLowerCase()]}
                alt={type}
              />
            ))}
          </TypesContainer>
        </InfoContainer>
      </Root>
    )
  }

  public pokemonClick = () => this.props.onPokemonClick(this.props.pokemon)

  public iconLoading = () => this.setState({ iconLoading: false })
}

export default PokemonItem
