import styled from 'styled-components'
import styledTS from 'styled-components-ts'

export interface IconProps {
  showed: boolean
}

const Icon = styledTS<IconProps>(styled.img)`
  height: 100%;
  width: auto;
  visibility: ${props => props.showed ? 'visible' : 'hidden'};
`

export default Icon
