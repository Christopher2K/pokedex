import * as React from 'react'

import Flex from '../Flex'
import Title from '../Title'
import Bar from './Bar'
import { Colors, Sizes } from '../../styles'
import { getStatName, getStatColor } from '../../lib'

const Root = Flex.Container.extend.attrs({
  direction: 'row',
  justify: 'flex-start',
  align: 'center'
})`
  width: 100%;
  margin-bottom: ${Sizes.size5};
`

const StatName = Title.H5.extend`
  color: ${Colors.black};
  width: 80px;
  max-width: 100px;
`

export interface StatInterface {
  min: number
  max: number
  value: number
  stat: Model.PokemonBaseStat
}

const Stat: React.SFC<StatInterface> = ({ min, max, value, stat }) => (
  <Root>
    <StatName align='left' marginless>{getStatName(stat)}</StatName>
    <Bar
      min={min}
      max={max}
      value={value}
      color={getStatColor(stat)}
    />
  </Root>
)

export default Stat
