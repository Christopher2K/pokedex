import * as React from 'react'
import styled, { css } from 'styled-components'
import styledTS from 'styled-components-ts'
import Color from 'color'

import { Colors, Sizes } from '../../styles'

const labelCss = css`
  position: absolute;
  bottom: 0;
  font-size: ${Sizes.size6};
`

const ValueLabel = styled.p`${labelCss}`
const MaxLabel = styled.p`${labelCss}`
const MinLabel = styled.p`${labelCss}`

const Level = styledTS<{width: number; color?: string}>(styled.div)`
  width: ${props => props.width}%;
  height: 100%;
  background-color: ${props => props.color};
  position: absolute;
  top: 0;
  left: 0;

  ${ValueLabel} {
    right: 0;
    transform: translate(50%, 120%);
  }
`

const Root = styledTS<{color?: string}>(styled.div)`
  width: 100%;
  height: 1.5rem;
  position: relative;
  border: 1px solid ${props => props.color};
  background-color: ${props => Color(props.color).fade(0.5).rgb().toString()};
  ${MinLabel} {
    left: 0;
    transform: translate(-50%, 120%);
  }

  ${MaxLabel} {
    right: 0;
    transform: translate(50%, 120%);
  }
`

interface BarProps {
  min: number
  max: number
  value: number
  color?: string
}

const Bar: React.SFC<BarProps> = ({ min, max, value, color }) => (
  <Root color={color}>
    <Level
      width={((value - min) / (max - min)) * 100}
      color={color}
    >
      <ValueLabel>{value}</ValueLabel>
    </Level>
    <MinLabel>{min}</MinLabel>
    <MaxLabel>{max}</MaxLabel>
  </Root>
)

Bar.defaultProps = {
  color: Colors.black
}

export default Bar
