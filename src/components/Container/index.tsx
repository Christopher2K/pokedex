import styled from 'styled-components'
import styledTS from 'styled-components-ts'

export interface Props {
  fluid?: boolean
}

const Container = styledTS<Props>(styled.div.attrs({
  className: (props: Props) => props.fluid ? 'container is-fluid' : 'container'
}))``

export default Container
