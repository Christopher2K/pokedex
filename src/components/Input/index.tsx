import styled from 'styled-components'

import { Colors } from '../../styles'

const Input = styled.input.attrs({
  className: 'input is-large'
})`
  width: 100%;
  border: 1px solid ${Colors.blazePurple};
  line-height: unset;

  &:focus {
    border-color: ${Colors.secondary};
  }
`

export default Input
