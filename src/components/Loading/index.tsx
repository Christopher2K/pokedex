import styledTS from 'styled-components-ts'
import styled from 'styled-components'

import loading from '../../assets/icons/loading.svg'
import { Sizes } from '../../styles'

export interface LoadingProps {
  iconSize: 'small' | 'normal' | 'large'
}

const Loading = styledTS<LoadingProps>(styled.img)`
  content: url("${loading}");
  width: ${props => {
    switch (props.iconSize) {
      case 'small': return Sizes.size3
      case 'normal': return Sizes.size2
      case 'large': return Sizes.size1
      default: return Sizes.size2
    }
  }};
  height: auto;
`

export default Loading
