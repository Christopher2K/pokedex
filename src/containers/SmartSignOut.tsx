import { connect, Dispatch } from 'react-redux'

import { SignOut } from '../components'
import { userLogout } from '../reducers/auth/actions'

export default connect(
  null,
  (dispatch: Dispatch<Type.Action>) => ({
    signOut: () => dispatch(userLogout())
  })
)(SignOut)
