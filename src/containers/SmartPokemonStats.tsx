import * as React from 'react'
import Pokedex from 'pokedex-promise-v2'
import Firebase from 'firebase/app'
import 'firebase/firestore'

import { PokemonStats, Flex, Loading } from '../components'

const RootLoading = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'center',
  align: 'center'
})`
  width: 100%;
`

interface SmartPokemonStatsState {
  loading: boolean
  typeDetails: Model.TypeDetails[]
  currentType?: Model.TypeDetails
}

interface SmartPokemonStatsProps {
  pokemon: Model.Pokemon
  details: Pokedex.Pokemon
}

class SmartPokemonStats extends React.Component<SmartPokemonStatsProps, SmartPokemonStatsState> {
  public state: SmartPokemonStatsState = {
    loading: true,
    typeDetails: []
  }

  public componentDidMount (): void {
    this.getPokemonTypesDetails(this.props.pokemon.types)
  }

  public render (): React.ReactNode {
    const { typeDetails, currentType } = this.state

    return (
      <>
        {currentType ? (
          <PokemonStats
            {...this.props}
            typeDetails={typeDetails}
            currentType={currentType}
            onTypeClicked={this.changeType}
          />
        ) : (
          <RootLoading>
            <Loading iconSize='large' />
          </RootLoading>
        )}
      </>
    )
  }

  public changeType = ({ target }: React.MouseEvent<HTMLInputElement>) => {
    const { name } = target as HTMLInputElement
    this.setState({
      currentType: this.state.typeDetails.find(type => type.name === name)
    })
  }

  private getPokemonTypesDetails = (types: string[]) => {
    const typePromises = types.map(type =>
      Firebase.firestore().collection('types')
        .doc(type)
        .get()
    )

    Promise.all(typePromises)
      .then(this.extractTypes)
      .catch(err => {
        console.error(err)
        window.alert('Error ! Try to reload the page')
        this.setState({ loading: false })
      })
  }

  private extractTypes = (data: Firebase.firestore.DocumentSnapshot[]) => {
    const typeDetails: Model.TypeDetails[] = []

    for (let document of data) {
      if (document.exists) {
        typeDetails.push({ name: document.id, ...document.data() } as Model.TypeDetails)
      }
    }

    this.setState({ loading: false, typeDetails, currentType: typeDetails[0] })
  }
}

export default SmartPokemonStats
