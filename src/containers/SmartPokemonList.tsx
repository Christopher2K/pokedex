import * as React from 'react'
import { connect } from 'react-redux'
import { History } from 'history'
import { withRouter } from 'react-router'
import { compose } from 'recompose'

import { PokemonList } from '../components'

namespace withPokemonList {
  export interface StateProps {
    pokemonList: Model.Pokemons
    searching: boolean
  }

  export interface OwnProps {
    history: History
  }

  export type HOCProps = StateProps & OwnProps

  export const hoc = connect<StateProps, null, OwnProps, State.Root>(
    state => ({
      pokemonList: state.search.pokemons,
      searching: state.search.searching
    }),
    null
  )
}

interface SmartPokemonListProps extends withPokemonList.HOCProps {
}

interface SmartPokemonListState {
  currentPage: number
}

class SmartPokemonList extends React.Component<SmartPokemonListProps, SmartPokemonListState> {
  public state: SmartPokemonListState = {
    currentPage: 1
  }

  private resultsPerPages: number = 15

  public render (): React.ReactNode {
    const { pokemonList, searching } = this.props
    const { currentPage } = this.state

    return (
      <PokemonList
        loading={searching}
        pokemonList={pokemonList.slice(0, currentPage * this.resultsPerPages)}
        maxItems={pokemonList.length}
        onNextClick={this.showNextPage}
        onPokemonClick={this.goToPokemonDetails}
      />
    )
  }

  public showNextPage = () => {
    if (this.state.currentPage * this.resultsPerPages < this.props.pokemonList.length) {
      this.setState({ currentPage: this.state.currentPage + 1 })
    }
  }

  public goToPokemonDetails = (pokemon: Model.Pokemon) => {
    const { history } = this.props
    history.push(`/app/pokemon/${pokemon.id}`)
  }
}

export default compose(
  withRouter,
  withPokemonList.hoc
)(SmartPokemonList)
