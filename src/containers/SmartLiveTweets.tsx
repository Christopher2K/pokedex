import * as React from 'react'
import IO from 'socket.io-client'

import { Tweet, Flex, Title, Message } from '../components'
import { API_URL } from '../settings'
import Env from '../environment'

const Root = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'flex-start'
})`
  width: 100%;
`

interface SmartLiveTweetsProps {
  limit?: number
  pokemon: Model.Pokemon
}

interface SmartLiveTweetsState {
  error: boolean
  tweets: Model.Tweet[]
}

class SmartLiveTweets extends React.Component<SmartLiveTweetsProps, SmartLiveTweetsState> {
  public static defaultProps: Partial<SmartLiveTweetsProps> = {
    limit: 5
  }

  public state: SmartLiveTweetsState = {
    error: false,
    tweets: []
  }

  private socket: SocketIOClient.Socket

  public componentDidMount (): void {
    this.subscribeToTweets()
  }

  public componentWillUnmount (): void {
    this.removeSocketConnection()
  }

  public render (): React.ReactNode {
    const { tweets, error } = this.state
    const { pokemon } = this.props
    return (
      <Root>
        <Title.H3>Last tweets about {pokemon.name}</Title.H3>
        {error && (
          <Message kind='danger'>Cannot connect to live tweet API !</Message>
        )}
        <div>
          {tweets.map(t => (
            <Tweet key={t.text} tweet={t} />
          ))}
        </div>
      </Root>
    )
  }

  private subscribeToTweets = () => {
    const { pokemon, limit } = this.props
    this.socket = IO(API_URL, {
      query: {
        term: pokemon.name
      }
    })

    this.socket.on('initial', ({ statuses }: any) => {
      if (statuses.length > limit!) {
        let tweets: Model.Tweet[] = []
        for (let i = 0; i < limit!; i++) {
          tweets.push({
            text: statuses[i].text,
            created_at: statuses[i].created_at,
            user: statuses[i].user.name,
            username: statuses[i].user.screen_name
          })
        }
        this.setState({ tweets })
      } else {
        this.setState({
          tweets: statuses.map((tweet: any) => ({
            text: tweet.text,
            timestamp: tweet.created_at,
            user: tweet.user.name,
            username: tweet.user.screen_name
          }))
        })
      }
    })

    this.socket.on('tweets', (data: any) => {
      this.setState({
        tweets: [
          {
            text: data.text,
            created_at: data.created_at,
            user: data.user.name,
            username: data.user.screen_name
          },
          ...this.state.tweets.slice(0, 4)
        ]
      })
    })

    this.socket.on('disconnect', () => {
      this.socket.removeAllListeners()
    })

    this.socket.on('error', (data: any) => {
      if (Env.dev) console.error(data)
      this.setState({ error: true })
    })

    this.socket.on('tweet_error', (data: any) => {
      if (Env.dev) console.error(data)
      this.setState({ error: true })
    })
  }

  private removeSocketConnection = () => {
    if (this.socket) {
      this.socket.close()
    }
  }
}

export default SmartLiveTweets
