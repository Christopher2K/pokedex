import * as React from 'react'

import { Navbar, NavbarItem } from '../components'
import { withUser } from '../behaviors'

interface SmartNavbarProps {
  user: Model.User
}

interface SmartNavbarState {
  isOpen: boolean
}

class SmartNavbar extends React.Component<SmartNavbarProps, SmartNavbarState> {
  public state: SmartNavbarState = {
    isOpen: false
  }

  render (): React.ReactNode {
    const { isOpen } = this.state
    const { user } = this.props

    return (
      <Navbar
        onBurgerClick={this.toggleNavbar}
        isOpen={isOpen}
      >
        <NavbarItem
          onClick={this.closeNavbarIfOpen}
          to='/app'
        >
          home
        </NavbarItem>
        {user ? (
          <>
            {/* <NavbarItem
              onClick={this.closeNavbarIfOpen}
              to='/app/bookmarks'
            >
              bookmarks
            </NavbarItem> */}
            <NavbarItem
              onClick={this.closeNavbarIfOpen}
              to='/signout'
            >
              sign out
            </NavbarItem>
          </>
        ) : (
          <NavbarItem
            onClick={this.closeNavbarIfOpen}
            to='/signin'
          >
            sign in
          </NavbarItem>
        )}
      </Navbar>
    )
  }

  public toggleNavbar = () => this.setState({ isOpen: !this.state.isOpen })

  public closeNavbarIfOpen = () => {
    if (this.state.isOpen) this.setState({ isOpen: false })
  }
}

export default withUser(SmartNavbar)
