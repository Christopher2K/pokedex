import * as React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import * as Firebase from 'firebase'

import { AuthMechanism, withLoginSuccess } from '../behaviors'
import { AuthForm } from '../components'
import { Colors, Breakpoints, Sizes } from '../styles'

const SignInLink = styled(Link)`
  color: ${Colors.white};
  font-size: ${Sizes.typo5};

  &:hover {
    color: ${Colors.white};
    text-decoration: underline;
  }

  ${Breakpoints.belowDesktop} {
    color: ${Colors.primary};

    &:hover {
      color: ${Colors.primary};
    }
  }
`

export interface SmartSignUpFormProps {
  onSuccess? (user: Firebase.User): void
}

const SmartSignUpForm: React.SFC<SmartSignUpFormProps> = ({ onSuccess }) => (
  <AuthMechanism
    authType='signup'
    onSuccess={onSuccess!}
    render={({
      email,
      password,
      loading,
      onSubmit,
      onChange,
      error
    }) => (
      <AuthForm
        {...{ email, password, loading, onSubmit, onChange, error }}
        optionComponent={<SignInLink to='/signin'>I already have an account</SignInLink>}
      />
    )}
  />
)

SmartSignUpForm.defaultProps = {
  onSuccess: user => console.log(user)
}

export default withLoginSuccess(SmartSignUpForm)
