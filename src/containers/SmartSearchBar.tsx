import * as React from 'react'
import { Location, History } from 'history'
import { withRouter } from 'react-router'
import qs from 'query-string'
import { Subscription } from 'rxjs/Subscription'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { debounceTime } from 'rxjs/operators'

import { Input } from '../components'

interface SmartSearchBarState {
  query: string
}

interface SmartSearchBarProps {
  history: History
  location: Location
}

class SmartSearchBar extends React.Component<SmartSearchBarProps, SmartSearchBarState> {
  public state: SmartSearchBarState = {
    query: ''
  }

  private inputRef: React.RefObject<HTMLInputElement>
  private inputChange$: BehaviorSubject<string>
  private subscription: Subscription

  public constructor (props: SmartSearchBarProps) {
    super(props)
    this.inputRef = React.createRef<HTMLInputElement>()
    this.inputChange$ = new BehaviorSubject<string>('')
  }

  public componentDidMount (): void {
    const { location } = this.props
    this.setState({
      query: qs.parse(location.search).query || ''
    })

    this.subscription = this.inputChange$
      .pipe(
        debounceTime(300)
      ).subscribe((query: string) => {
        this.props.history.push(`/app?${qs.stringify({ query })}`)
      })
  }

  public componentDidUpdate (_: any, prevState: SmartSearchBarState): void {
    if (prevState.query !== this.state.query) {
      this.inputChange$.next(this.state.query)
    }
  }

  public componentWillUnmount (): void {
    this.subscription.unsubscribe()
  }

  public render (): React.ReactNode {
    const { query } = this.state
    return (
      <Input
        innerRef={this.inputRef}
        placeholder='e.g. Pikachu'
        value={query}
        onChange={this.onInputChange}
      />
    )
  }

  private onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => this.setState({ query: event.target.value })
}

export default withRouter(({ location, history }) => <SmartSearchBar location={location} history={history} />)
