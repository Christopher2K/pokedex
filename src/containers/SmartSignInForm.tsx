import * as React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import * as Firebase from 'firebase'

import { AuthMechanism, withLoginSuccess } from '../behaviors'
import { AuthForm } from '../components'
import { Colors, Breakpoints, Sizes } from '../styles'

const SignUpLink = styled(Link)`
  color: ${Colors.white};
  font-size: ${Sizes.typo5};

  &:hover {
    color: ${Colors.white};
    text-decoration: underline;
  }

  ${Breakpoints.belowDesktop} {
    color: ${Colors.primary};

    &:hover {
      color: ${Colors.primary};
    }
  }
`

export interface SmartSignInFormProps {
  onSuccess? (user: Firebase.User): void
}

const SmartSignInForm: React.SFC<SmartSignInFormProps> = ({ onSuccess }) => (
  <AuthMechanism
    authType='signin'
    onSuccess={onSuccess!}
    render={({
      email,
      password,
      loading,
      onSubmit,
      onChange,
      error
    }) => (
      <AuthForm
        {...{ email, password, loading, onSubmit, onChange, error }}
        optionComponent={<SignUpLink to='/signup'>Sign Up</SignUpLink>}
      />
    )}
  />
)

SmartSignInForm.defaultProps = {
  onSuccess: user => console.log(user)
}

export default withLoginSuccess(SmartSignInForm)
