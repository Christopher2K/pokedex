export default {
  prod: process.env.NODE_ENV === 'production',
  dev: process.env.NODE_ENV !== 'production'
}
