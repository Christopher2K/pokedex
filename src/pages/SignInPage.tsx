import * as React from 'react'
import styledTS from 'styled-components-ts'
import { Link } from 'react-router-dom'

import {
  UserAccountLayout,
  Grid,
  Title,
  Button
} from '../components'
import { SmartSignInForm } from '../containers'
import { Colors, Breakpoints } from '../styles'

export interface SignInPageProps {}

const FormTitle = styledTS(Title.H1.extend)`
  color: ${Colors.white};

  ${Breakpoints.belowDesktop} {
    color: ${Colors.primary};
  }
`

const SignInPage: React.SFC<SignInPageProps> = () => (
  <UserAccountLayout>
    <Grid.Container className='is-centered'>
      <Grid.Column
        columnSize='6'
      >
        <FormTitle>Sign In</FormTitle>
        <SmartSignInForm />
        <Link to='/app'>
          <Button>Take me to the app</Button>
        </Link>
      </Grid.Column>
    </Grid.Container>
  </UserAccountLayout>
)

export default SignInPage
