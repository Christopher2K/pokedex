import * as React from 'react'
import styled from 'styled-components'

import { Flex, Grid, Title } from '../components'
import { SmartSearchBar, SmartPokemonList } from '../containers'
import { Colors } from '../styles'

const Root = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'flex-start'
})`
  width: 100%;
`

const PageTitle = Title.H2.extend`
  color: ${Colors.primary};
  width: 100%;
`

const SearchContainer = styled(({ className, children }: any) => (
  <Grid.Container className={`${className} is-centered`}>
    <Grid.Column columnSize='5'>
      {children}
    </Grid.Column>
  </Grid.Container>
))``

const ListContainer = styled(({ className, children }: any) => (
  <Grid.Container className={`${className} is-centered`}>
    <Grid.Column columnSize='8'>
      {children}
    </Grid.Column>
  </Grid.Container>
))``

const HomePage: React.SFC = () => (
  <Root>
    <PageTitle align='center'>Find your favorite pokemon !</PageTitle>
    <SearchContainer>
      <SmartSearchBar />
    </SearchContainer>
    <ListContainer>
      <SmartPokemonList />
    </ListContainer>
  </Root>
)

export default HomePage
