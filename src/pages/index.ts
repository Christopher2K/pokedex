export { default as SignInPage } from './SignInPage'
export { default as SignUpPage } from './SignUpPage'
export { default as HomePage } from './HomePage'
export { default as LoadingPage } from './LoadingPage'
export { default as PokemonDetailsPage } from './PokemonDetailsPage'
export { default as NotFound } from './NotFound'
export { default as ErrorPage } from './ErrorPage'
