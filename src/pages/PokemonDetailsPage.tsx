import * as React from 'react'
import { match } from 'react-router'

import { PokemonDetailsData } from '../behaviors'
import { Flex, Grid, Loading } from '../components'
import { SmartPokemonCard, SmartPokemonStats, SmartLiveTweets } from '../containers'

export interface PokemonDetailsPageProps {
  match: match<{id: string}>
}

const Root = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'flex-start',
  align: 'flex-start'
})`
  width: 100%;
`

const LoadingContainer = Grid.Column.extend`
  display: flex;
  justify-content: center;
  align-items: center;
`

const PokemonDetailsPage: React.SFC<PokemonDetailsPageProps> = ({ match }) => (
  <Root>
    <Grid.Container className='is-centered'>
      <PokemonDetailsData
        pokemonId={match.params.id}
        render={({ loading, pokemon, details }) => (
          <>
            {loading ? (
              <LoadingContainer columnSize='12'>
                <Loading iconSize='large' />
              </LoadingContainer>
            ) : (
              <>
                <Grid.Column columnSize='4'>
                  <SmartPokemonCard
                    pokemon={pokemon}
                    details={details}
                  />
                </Grid.Column>
                <Grid.Column columnSize='8'>
                  <SmartPokemonStats
                    pokemon={pokemon}
                    details={details}
                  />
                  <SmartLiveTweets
                    pokemon={pokemon}
                  />
                </Grid.Column>
              </>
            )}
          </>
        )}
      />
    </Grid.Container>
  </Root>
)

export default PokemonDetailsPage
