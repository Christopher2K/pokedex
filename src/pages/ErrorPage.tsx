import * as React from 'react'
import { Link } from 'react-router-dom'

import { Flex, Title, Button } from '../components'

const Root = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'center',
  align: 'center'
})`
  width: 100%;
  height: 100%;
`

const LoadingPage: React.SFC = () => (
  <Root>
    <Title.H4>App has crashed !!!!</Title.H4>
    <Title.H1>Ooooh nooooooooo !!!!</Title.H1>
    <Link to='/app'>
      <Button btnSize='large'>Take me back (again) to the app</Button>
    </Link>
  </Root>
)

export default LoadingPage
