import * as React from 'react'
import styledTS from 'styled-components-ts'
import { Link } from 'react-router-dom'

import {
  UserAccountLayout,
  Grid,
  Title,
  Button
} from '../components'
import { SmartSignUpForm } from '../containers'
import { Colors, Breakpoints } from '../styles'

interface SignUpPageProps {}

const FormTitle = styledTS(Title.H1.extend)`
  color: ${Colors.white};

  ${Breakpoints.belowDesktop} {
    color: ${Colors.primary};
  }
`

const SignUpPage: React.SFC<SignUpPageProps> = () => (
  <UserAccountLayout>
    <Grid.Container className='is-centered'>
      <Grid.Column
        columnSize='6'
      >
        <FormTitle>Sign Up</FormTitle>
        <SmartSignUpForm />
        <Link to='/app'>
          <Button>Take me to the app</Button>
        </Link>
      </Grid.Column>
    </Grid.Container>
  </UserAccountLayout>
)

export default SignUpPage
