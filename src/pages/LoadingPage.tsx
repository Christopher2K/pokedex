import * as React from 'react'

import { Flex, Loading } from '../components'

const Root = Flex.Container.extend.attrs({
  direction: 'column',
  justify: 'center',
  align: 'center'
})`
  width: 100%;
  height: 100vh;
`

const LoadingPage: React.SFC = () => (
  <Root>
    <Loading iconSize='large' />
  </Root>
)

export default LoadingPage
