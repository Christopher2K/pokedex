declare namespace State {
  interface Router {
    location: any
  }

  interface Auth {
    user?: Model.User
    authenticating: boolean
  }

  interface Search {
    pokemons: Model.Pokemons
    searching: boolean
  }

  interface Root {
    router: Router
    auth: Auth
    search: Search
  }
}
