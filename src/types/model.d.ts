declare namespace Model {
  interface User {
    id: string,
    email: string
  }

  interface Pokemon {
    id: string
    name: string
    types: [string]
    icon: string
    artwork: string
  }

  type Pokemons = Pokemon[]

  type PokemonBaseStat = 'attack' | 'defense' | 'special-attack' | 'special-defense' | 'speed' | 'hp'

  type MinMax = { min: number, max: number }

  interface TypeDetails {
    name: string
    hp: MinMax
    attack: MinMax
    defense: MinMax
    ['special-attack']: MinMax
    ['special-defense']: MinMax
    speed: MinMax
  }

  interface Tweet {
    text: string
    user: string
    username: string
    created_at: string
  }
}
