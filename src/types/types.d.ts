declare namespace Type {
  interface Field<T = string> {
    value: T
    error?: string
  }

  type Fields = Field[]

  interface Action<T = any> {
    type: string
    payload?: T
  }

  type ActionFactory<T = any> = (payload?: T) => Action<T>
}
