/**
 * User breakpoints
 */
export const isMobile: string = '@media (max-width: 768px)'
export const isTablet: string = '@media (min-width: 769px) and (max-width: 1023px)'
export const isDesktop: string = '@media (min-width: 1024px)'

export const atLeastTablet: string = '@media (min-width: 769px)'

export const belowDesktop: string = '@media (max-width: 1023px)'

export default {
  isMobile,
  isTablet,
  isDesktop,
  atLeastTablet,
  belowDesktop
}
