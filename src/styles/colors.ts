const white = '#FFFFFF'
const black = '#000000'

const lightGrey = '#DEDEDE'
const grey = '#AAAAAA'
const darkGrey = '#808080'

const info = '#349FE8'
const success = '#3BCD6A'
const warning = '#FDDB6B'
const danger = '#F93F66'

/**
 * Other Colors (CUSTOM)
 */
const skizzy = '#6C71D5'
const seaBlue = '#51A4ED'
const blazePurple = '#595C95'

const primary = skizzy
const secondary = seaBlue

export default {
  white,
  black,

  lightGrey,
  grey,
  darkGrey,

  info,
  success,
  warning,
  danger,

  primary,
  secondary,

  blazePurple
}
