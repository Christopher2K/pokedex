const radius = '5px'
const shadow = '0 0 3px 0 rgba(0,0,0,0.15)'

export default {
  radius,
  shadow
}
