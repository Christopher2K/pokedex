/**
 * Typography sizes in REM
 * Used for font-size
 */
const typo1 = '3rem'
const typo2 = '2.5rem'
const typo3 = '2rem'
const typo4 = '1.5rem'
const typo5 = '1.25rem'
const typo6 = '1rem'
const typo7 = '0.75rem'
const typo8 = '0.5rem'

/**
 * Spaces sizes in REM
 * Used for padding & margin
 */
const size1 = '4.5rem'
const size2 = '3.5rem'
const size3 = '2.5rem'
const size4 = '1.8rem'
const size5 = '1.3rem'
const size6 = '0.7rem'
const size7 = '0.5rem'

export default {
  size1,
  size2,
  size3,
  size4,
  size5,
  size6,
  size7,

  typo1,
  typo2,
  typo3,
  typo4,
  typo5,
  typo6,
  typo7,
  typo8
}
