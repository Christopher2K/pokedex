import * as React from 'react'
import { ConnectedRouter } from 'react-router-redux'
import { History } from 'history'
import { Switch, Route } from 'react-router-dom'

import { AppLayout, Container } from '../components'
import { SmartNavbar, SmartSignOut } from '../containers'
import * as Pages from '../pages'
import * as Guards from '../routing/guards'
import { AuthenticatingGuard, ErrorCatcher } from '../behaviors'

export interface Props {
  history: History
}

const Router: React.SFC<Props> = ({
  history
}) => (
  <ConnectedRouter history={history}>
    <ErrorCatcher>
      <AuthenticatingGuard>
        <Switch>
          <Route exact path='/' component={Guards.withAnonymousUserRequired(Pages.SignInPage)} />
          <Route exact path='/signin' component={Guards.withAnonymousUserRequired(Pages.SignInPage)} />
          <Route exact path='/signup' component={Guards.withAnonymousUserRequired(Pages.SignUpPage)} />
          <Route exact path='/signout' component={Guards.withUserRequired(SmartSignOut)} />
          <Route path='/app' render={
            (props) => (
              <AppLayout>
                <SmartNavbar {...props} />
                <Container>
                  <Switch>
                    <Route exact path='/app' component={Pages.HomePage} />
                    <Route exact path='/app/pokemon/:id' component={Pages.PokemonDetailsPage} />
                    {/* <Route exact path='/app/bookmarks' component={Guards.withUserRequired(() => <div>Bookmarks</div>)} /> */}
                  </Switch>
                </Container>
              </AppLayout>
            )
          } />
          <Route exact path='/error' component={Pages.ErrorPage} />
          <Route exact path='/not-found' component={Pages.NotFound} />
          <Route component={Pages.NotFound} />
        </Switch>
      </AuthenticatingGuard>
    </ErrorCatcher>
  </ConnectedRouter>
)

export default Router
