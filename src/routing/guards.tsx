import * as React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

export function withAnonymousUserRequired (WrappedComponent: React.ComponentType): React.ComponentType {
  return connect(
    (state: State.Root) => ({
      user: state.auth.user
    }),
    null
  )(({ user, ...props }: any) => (
    <>
    {user ? <Redirect to='/app' /> : <WrappedComponent {...props } />}
    </>
  ))
}

export function withUserRequired (WrappedComponent: React.ComponentType): React.ComponentType {
  return connect(
    (state: State.Root) => ({
      user: state.auth.user
    }),
    null
  )(({ user, ...props }: any) => (
    <>
    {!user ? <Redirect to='/signin' /> : <WrappedComponent {...props } />}
    </>
  ))
}
