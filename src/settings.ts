import Env from './environment'

const DEV_API_URL: string = 'http://localhost:3001'
const PROD_API_URL: string = 'https://pacific-ridge-60081.herokuapp.com/'

export const API_URL: string = Env.dev ? DEV_API_URL : PROD_API_URL
