import * as React from 'react'
import { Redirect } from 'react-router-dom'
import { withRouter } from 'react-router'

interface ErrorCatcherState {
  error: boolean
}

/**
 * Redirect to the error page if a render error occurs
 */
class ErrorCatcher extends React.Component<any, ErrorCatcherState> {
  public state: ErrorCatcherState = {
    error: false
  }

  public componentDidCatch (error: Error): void {
    console.error(error)
    this.setState({ error: true })
  }

  public render (): React.ReactNode {
    const { error } = this.state
    const { children } = this.props
    return (
      <>
      {error ? (
        <Redirect to='/error' />
      ) : (
        <>{children}</>
      )}
      </>
    )
  }
}

export default withRouter(ErrorCatcher) // to cause en re-render on router change since the whole router tree depends on this guard
