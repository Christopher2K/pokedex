import * as React from 'react'
import Firebase from 'firebase/app'
import 'firebase/firestore'
import { Redirect } from 'react-router'
import Pokedex from 'pokedex-promise-v2'

import Env from '../environment'

interface RenderProps {
  loading: boolean
  pokemon: Model.Pokemon
  details: Pokedex.Pokemon
}

export interface PokemonDetailsDataState {
  loading: boolean
  notFound: boolean
  pokemon?: Model.Pokemon
  details?: Pokedex.Pokemon
}

export interface PokemonDetailsDataProps {
  pokemonId: string
  render (props: RenderProps): React.ReactNode
}

/**
 * Render Props components that give all pokemon data corresponding to the ID passed in props
 */
class PokemonDetailsData extends React.Component<PokemonDetailsDataProps, PokemonDetailsDataState> {
  public state: PokemonDetailsDataState = {
    loading: true,
    notFound: false
  }

  private pokedex: Pokedex

  public componentDidMount (): void {
    const { pokemonId } = this.props
    if (isNaN(parseInt(pokemonId, 10))) {
      return this.set404Error(null, `PokemonId ${pokemonId} not a number`)
    }

    this.pokedex = new Pokedex({
      protocol: 'https',
      timeout: 60000
    })
    this.getPokemons(pokemonId)
  }

  public render (): React.ReactNode {
    const { loading, pokemon, notFound, details } = this.state

    return (
      <>
        {notFound ? <Redirect to='/app/not-found' /> : this.props.render({ loading, pokemon: pokemon!, details: details! })}
      </>
    )
  }

  public getPokemons (pokemonId: string): void {
    const pokemonPromise = Firebase
      .firestore()
      .collection('pokemons')
      .doc(pokemonId)
      .get()

    const detailsPromise = this.pokedex.getPokemonByName(pokemonId)

    Promise.all([ pokemonPromise, detailsPromise ])
      .then(this.extractData)
      .catch(this.set404Error)
  }

  public extractData = ([pokemon, details]: [Firebase.firestore.DocumentSnapshot, Pokedex.Pokemon]) => {
    if (!pokemon.exists) {
      return this.set404Error(null, `pokemon ID ${this.props.pokemonId} not found in Firebase !`)
    }

    this.setState({
      pokemon: {
        id: pokemon.id,
        ...pokemon.data()
      } as Model.Pokemon,
      details,
      loading: false
    })
  }

  public set404Error = (error?: any, message?: string) => {
    if (error != null && Env.dev) console.error(error)
    if (message != null && Env.dev) console.error(message)

    this.setState({ notFound: true })
  }
}

export default PokemonDetailsData
