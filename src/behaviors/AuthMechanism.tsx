import * as React from 'react'
import * as Validators from 'validator'
import Firebase from 'firebase/app'
import 'firebase/auth'

interface RenderProps {
  email: Type.Field
  password: Type.Field
  loading?: boolean
  error?: string
  onSubmit (evt: React.FormEvent<HTMLFormElement>): void
  onChange (evt: React.ChangeEvent<HTMLInputElement>): void
}

interface AuthMechanismState {
  email?: Type.Field
  password?: Type.Field
  loading?: boolean
  error?: string
}

interface AuthMechanismProps {
  authType: 'signin' | 'signup'
  render: (props: RenderProps) => React.ReactNode
  onSuccess (user: Firebase.User): void
}

/**
 * Render Props component
 * Isolate the signin / signup behavior for the forms components
 */
class AuthMechanism extends React.Component<AuthMechanismProps, AuthMechanismState> {
  public state: AuthMechanismState = {
    email: { value: '' },
    password: { value: '' },
    loading: false
  }

  public render (): React.ReactNode {
    const { email, password, loading, error } = this.state
    return this.props.render({
      email: email!,
      password: password!,
      loading,
      onChange: this.onChange,
      onSubmit: this.onSubmit,
      error
    })
  }

  public onChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    const name: keyof AuthMechanismState = target.name as keyof AuthMechanismState
    this.setState({
      [name]: {
        value: target.value,
        ...this.validateFields(name, target.value)
      }
    })
  }

  public onSubmit = (evt: React.FormEvent<HTMLFormElement>) => {
    evt.preventDefault()
    const { email, password } = this.state
    const { authType, onSuccess } = this.props

    if (!email || !password) return

    const auth: Promise<Firebase.auth.UserCredential> = authType === 'signin' ?
      Firebase.auth().signInWithEmailAndPassword(email.value, password.value) :
      Firebase.auth().createUserWithEmailAndPassword(email.value, password.value)

    this.setState({ loading: true, error: undefined }, () => {
      auth.then(user => onSuccess(user.user!))
      .catch(err => {
        console.log(err)
        let error
        switch (err.code) {
          case 'auth/user-not-found': error = 'Email ou mot de passe incorrect'
            break
          default: error = 'Une erreur est survenue'
        }

        this.setState({ loading: false, error, password: { value: '' } })
      })
    })
  }

  private validateFields = (field: keyof AuthMechanismState, value: any) => {
    switch (field) {
      case 'email':
        return Validators.isEmail(value) ? {} : { error: `Cette adresse n'est pas valide` }
      case 'password':
        return {}
      default:
        throw new Error('Unknow form field')
    }
  }
}

export default AuthMechanism
