import { connect, Dispatch } from 'react-redux'
import Firebase from 'firebase/app'
import { History } from 'history'
import { withRouter } from 'react-router'
import { compose } from 'recompose'

import { userLogin } from '../reducers/auth/actions'

export interface DispatchProps {
  onSuccess (user: Firebase.User): void
}

export interface OwnProps {
  history: History
}

/**
 * After loggin action HOC
 */
const withLoginSuccess = connect<null, DispatchProps, OwnProps, State.Root>(
  null,
  (dispatch: Dispatch<Type.Action>, ownProps: OwnProps) => ({
    onSuccess: (user: Firebase.User) => {
      dispatch(userLogin({ id: user.uid, email: user.email! }))
      ownProps.history.push('/app')
    }
  })
)

export default compose(
  withRouter,
  withLoginSuccess
)
