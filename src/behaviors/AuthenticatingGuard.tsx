import * as React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { compose } from 'recompose'

import { LoadingPage } from '../pages'

namespace withAuthenticatingStatus {
  export interface StateProps {
    authenticating: boolean
  }

  export const hoc = connect<StateProps, null, any, State.Root>(
    (state: State.Root) => ({
      authenticating: state.auth.authenticating
    }),
    null
  )
}

interface AuthenticatingGuard extends withAuthenticatingStatus.StateProps {
  children: React.ReactNode
}

/**
 * Render a loading component if the app is trying to authenticate
 */
const AuthenticatingGuard: React.SFC<AuthenticatingGuard> = ({
  authenticating,
  children,
  ...props
}) => (
  <>
    {authenticating ? <LoadingPage /> : children}
  </>
)

export default compose(
  withRouter, // to cause en re-render on router change since the whole router tree depends on this guard
  withAuthenticatingStatus.hoc
)(AuthenticatingGuard)
