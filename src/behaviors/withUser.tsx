import { connect } from 'react-redux'

export interface StateProps {
  user?: Model.User
}

/**
 * User Data HOC
 */
const withUser = connect<StateProps, null, any, State.Root>(
  (state: State.Root) => ({
    user: state.auth.user
  }),
  null
)

export default withUser
