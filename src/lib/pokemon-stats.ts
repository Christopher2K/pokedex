export function getStatName (stat: Model.PokemonBaseStat): string {
  switch (stat) {
    case 'hp': return 'HP'
    case 'attack': return 'ATK'
    case 'defense': return 'DEF'
    case 'special-attack': return 'SPE A.'
    case 'special-defense': return 'SPE D.'
    case 'speed': return 'SPD'
    default: return 'UNK'
  }
}

export function getStatColor (stat: Model.PokemonBaseStat): string {
  switch (stat) {
    case 'hp': return '#E05959'
    case 'attack': return '#44C79F'
    case 'defense': return '#BC7A39'
    case 'special-attack': return '#69C14F'
    case 'special-defense': return '#2F72DE'
    case 'speed': return '#59ACE0'
    default: return '#000000'
  }
}
