export function fieldsAreClean (fields: Type.Fields): boolean {
  const { noError, emptyForm } = fields.reduce(
    ({ noError, emptyForm }, field: Type.Field) =>
      ({
        noError: !field.error && noError,
        emptyForm: emptyForm || field.value == null || (field.value.length && field.value.length === 0)
      }),
    { noError: true, emptyForm: false })
  return noError && !emptyForm
}
