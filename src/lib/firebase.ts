import { Observable } from 'rxjs/Observable'
import { Subscriber } from 'rxjs/Subscriber'
import Firebase from 'firebase/app'
import 'firebase/auth'

export function userLoggedIn$ (): Observable<Firebase.User | null> {
  return Observable.create((subscriber: Subscriber<any>) => {
    const unsubscribe = Firebase.auth().onAuthStateChanged(user => {
      if (user != null) {
        subscriber.next(user)
        subscriber.complete()
      } else {
        subscriber.next(null)
        subscriber.complete()
      }
      unsubscribe()
    })
  })
}
