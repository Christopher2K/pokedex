import * as React from 'react'
import { render } from 'react-dom'
import { applyMiddleware, createStore, Middleware, Store } from 'redux'
import { Provider } from 'react-redux'
import logger from 'redux-logger'
import { createBrowserHistory, History } from 'history'
import Firebase from 'firebase/app'
import { createEpicMiddleware } from 'redux-observable'

import Router from './routing/Router'
import Env from './environment'
import { rootEpic, rootReducer } from './reducers'

// Firebase init
if (Firebase.apps.length === 0) {
  Firebase.initializeApp({
    apiKey: 'AIzaSyAgduIPEy4J3TH09gK2fmqpCQ-lQcm-6vk',
    authDomain: 'zen-pokedex.firebaseapp.com',
    databaseURL: 'https://zen-pokedex.firebaseio.com',
    projectId: 'zen-pokedex',
    storageBucket: 'zen-pokedex.appspot.com',
    messagingSenderId: '379193181385'
  })

  Firebase.firestore().settings({ timestampsInSnapshots: true })
}

// Redux & Router setup
const middlewares: Middleware<any, State.Root, any>[] = Env.prod ? [
  createEpicMiddleware(rootEpic)
] : [
  createEpicMiddleware(rootEpic),
  logger
]

const store: Store<State.Root> = createStore(
  rootReducer,
  applyMiddleware(...middlewares)
)
const history: History = createBrowserHistory()

render(
  <Provider store={store}>
    <Router history={history} />
  </Provider>,
  document.querySelector('#root')
)
