import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { combineEpics } from 'redux-observable'

import * as fromAuth from './auth'
import * as fromSearch from './search'

export const rootReducer = combineReducers({
  router: routerReducer,
  auth: fromAuth.reducer,
  search: fromSearch.reducer
})

export const rootEpic = combineEpics(
  fromAuth.epics,
  fromSearch.epics
)
