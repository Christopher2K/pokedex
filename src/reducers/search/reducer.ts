import Types from './actions'

export const initialState: State.Search = {
  pokemons: [],
  searching: true
}

export function reducer (state: State.Search = initialState, action: Type.Action): State.Search {
  switch (action.type) {
    case Types.SEARCH_POKEMONS: return { ...state, pokemons: [], searching: true }
    case Types.SET_POKEMONS: return { ...state, pokemons: action.payload, searching: false }
    case Types.CLEAR_RESULTS: return { ...initialState }
    default: return state
  }
}
