import { ActionsObservable, combineEpics } from 'redux-observable'
import { switchMap, filter, distinctUntilChanged, map } from 'rxjs/operators'
import { fromPromise } from 'rxjs/observable/fromPromise'
import qs from 'query-string'
import Firebase from 'firebase/app'
import 'firebase/firestore'

import Actions, * as SearchAction from './actions'
import { capitalizeFirstLetter } from '../../lib/string'

function _mapToQuerySnapshotToData (snapshot: Firebase.firestore.QuerySnapshot): Model.Pokemons {
  const docs = snapshot.docs
  return docs.map(doc => ({ id: doc.id, ...doc.data() }) as Model.Pokemon)
}

const triggerSearch$ = (actions$: ActionsObservable<Type.Action>) =>
  actions$.ofType('@@router/LOCATION_CHANGE')
    .pipe(
      filter(({ payload }) => payload.pathname === '/app' && payload.search.includes('query')),
      distinctUntilChanged((x, y) => x.payload.search === y.payload.search),
      map(({ payload }) => SearchAction.searchPokemon(qs.parse(payload.search).query))
    )

const getResults$ = (actions$: ActionsObservable<Type.Action>) =>
  actions$.ofType(Actions.SEARCH_POKEMONS)
    .pipe(
      switchMap(({ payload }) => {
        console.log(payload)
        if (payload.length === 0) {
          return fromPromise(
            Firebase.firestore()
              .collection('pokemons')
              .orderBy('name', 'asc')
              .get()
          )
        } else {
          return fromPromise(
            Firebase.firestore()
              .collection('pokemons')
              .where('name', '>=', capitalizeFirstLetter(payload))
              .get()
          )
        }
      }),
      map((result: Firebase.firestore.QuerySnapshot) => {
        const pokemons: Model.Pokemons = _mapToQuerySnapshotToData(result)
        return SearchAction.setPokemons(pokemons)
      })
    )

export default combineEpics<any>(
  triggerSearch$,
  getResults$
)
