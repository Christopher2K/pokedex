const SEARCH_POKEMONS: string = '[@pokemons] Search a pokemon by pattern'
const SET_POKEMONS: string = '[@pokemons] Set pokemons that match the pattern'
const CLEAR_RESULTS: string = '[@pokemons] Clear search results'

export const searchPokemon: Type.ActionFactory<string> = query => ({
  type: SEARCH_POKEMONS,
  payload: query
})

export const setPokemons: Type.ActionFactory<Model.Pokemons> = pokemons => ({
  type: SET_POKEMONS,
  payload: pokemons
})

export const clearResults: Type.ActionFactory = () => ({
  type: CLEAR_RESULTS
})

export default {
  SEARCH_POKEMONS,
  SET_POKEMONS,
  CLEAR_RESULTS
}
