const AUTH_DONE: string = '[@auth] Initial authentication process have been done'
const USER_LOGIN: string = '[@auth] User now logged in'
const USER_LOGOUT: string = '[@auth] User logged out'

export const authDone: Type.ActionFactory = () => ({
  type: AUTH_DONE
})

export const userLogin: Type.ActionFactory<Model.User> = payload => ({
  payload,
  type: USER_LOGIN
})

export const userLogout: Type.ActionFactory = () => ({
  type: USER_LOGOUT
})

export default {
  AUTH_DONE,
  USER_LOGIN,
  USER_LOGOUT
}
