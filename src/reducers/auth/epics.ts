import { ActionsObservable, combineEpics } from 'redux-observable'
import Firebase from 'firebase/app'
import 'firebase/auth'
import { first, switchMap, ignoreElements } from 'rxjs/operators'
import { fromPromise } from 'rxjs/observable/fromPromise'
import { of } from 'rxjs/observable/of'

import Actions, * as AuthActions from './actions'
import { userLoggedIn$ } from '../../lib'

const authenticatingAtStart$ = (actions$: ActionsObservable<Type.Action>) =>
  actions$
    .pipe(
      first(),
      switchMap(userLoggedIn$),
      switchMap((user: Firebase.User) =>
        user != null ?
          of(AuthActions.userLogin({ email: user.email!, id: user.uid }), AuthActions.authDone()) :
          of(AuthActions.authDone())
      )
    )

const firebaseSignout$ = (actions$: ActionsObservable<Type.Action>) =>
  actions$
    .ofType(Actions.USER_LOGOUT)
    .pipe(
      switchMap(() => fromPromise(Firebase.auth().signOut())),
      ignoreElements()
    )

export default combineEpics(
  authenticatingAtStart$,
  firebaseSignout$
)
