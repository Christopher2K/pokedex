import Types from './actions'

export const initialState: State.Auth = {
  authenticating: true
}

export function reducer (state: State.Auth = initialState, action: Type.Action): State.Auth {
  switch (action.type) {
    case Types.AUTH_DONE: return { ...state, authenticating: false }
    case Types.USER_LOGIN: return { ...state, user: action.payload }
    case Types.USER_LOGOUT: return { ...initialState, authenticating: false }
    default: return state
  }
}
