import firebase_admin
from firebase_admin import credentials, firestore
import csv

cred = credentials.Certificate('./service_account_key.json')
firebase_admin.initialize_app(cred)
db = firestore.client()

"""
e.g. -> type_with_all_stats_values = [
    {
        'name': 'Speed'
        'hp': []
        'attack': []
        'defense': []
        'special_attack': []
        'special_defense': []
        'speed': []
    }
    ...
]
"""
type_with_all_stats_values = []

with open('pokemon_v2.csv', newline='') as pokemons:
    reader = csv.reader(pokemons, delimiter=',')

    for index, row in enumerate(reader):
        if index == 0: continue
        current_types = [x for x in [row[2], row[3]] if len(x) > 0]
        hp = int(row[5])
        attack = int(row[6])
        defense = int(row[7])
        special_attack = int(row[8])
        special_defense = int(row[9])
        speed = int(row[10])

        for pokemon_type in current_types:
            # Check if the type does exists in the main list
            exists = False
            founded_item_index = None
            for index, type in enumerate(type_with_all_stats_values):
                if type['name'] == pokemon_type:
                    exists = True
                    founded_item_index = index
            
            if not exists:
                # Create an occurence for that type with data that we need
                type_with_all_stats_values.append({
                    'name': pokemon_type,
                    'hp': [hp],
                    'attack': [attack],
                    'defense': [defense],
                    'special-attack': [special_attack],
                    'special-defense': [special_defense],
                    'speed': [speed],
                })
                
            else:
                # Modify the occurence with new data for current pokemon
                type_with_all_stats_values[founded_item_index]['hp'].append(hp)
                type_with_all_stats_values[founded_item_index]['attack'].append(attack)
                type_with_all_stats_values[founded_item_index]['defense'].append(defense)
                type_with_all_stats_values[founded_item_index]['special-attack'].append(special_attack)
                type_with_all_stats_values[founded_item_index]['special-defense'].append(special_defense)
                type_with_all_stats_values[founded_item_index]['speed'].append(speed)

# Format data for batch insert
batch = db.batch()
for index, type in enumerate(type_with_all_stats_values):
    ref = db.collection(u'types').document(u'{}'.format(type['name']))

    batch.set(ref, {
        u'hp': {'min': min(type['hp']), 'max': max(type['hp']) },
        u'attack': {'min': min(type['attack']), 'max': max(type['attack']) },
        u'defense': {'min': min(type['defense']), 'max': max(type['defense']) },
        u'special-attack': {'min': min(type['special-attack']), 'max': max(type['special-attack']) },
        u'special-defense': {'min': min(type['special-defense']), 'max': max(type['special-defense']) },
        u'speed': {'min': min(type['speed']), 'max': max(type['speed']) }
    })

batch.commit()

print(u'Done')
