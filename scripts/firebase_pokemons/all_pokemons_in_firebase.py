import firebase_admin
from firebase_admin import credentials, firestore
import csv
import re

cred = credentials.Certificate('./service_account_key.json')
firebase_admin.initialize_app(cred)

db = firestore.client()
pokemons_list = []
types_list = []

# Open the CSV File
with open('pokemon_v2.csv', newline='') as pokemons:
    reader = csv.reader(pokemons, delimiter=',')

    # Intialization firestore batch
    # Formatting my pokemons array
    for index, row in enumerate(reader):
        # Skipping first line
        if index == 0: continue

        number = row[0]
        name = ' '.join(re.findall('[A-Z][^A-Z]*', row[1].replace(' ', '').replace('-', '')))
        types = [x for x in [row[2], row[3]] if len(x) > 0]

        for type in types:
            if type not in types_list:
                types_list.append(type)

        already_in_list = False

        for pok in pokemons_list:
            for attr, value in pok.items():
                if attr == 'number' and value == number:
                    already_in_list = True
                    break

        if not already_in_list:
            pokemons_list.append({ 'number': number, 'name': name, 'types': types })

# Splitting array into smaller array for multiple batch operations
split_arrray = lambda arr, target_length = 3: [ arr[i: i+target_length] for i in range(0, len(arr), target_length) ]
multiple_pokemons_lists = split_arrray(pokemons_list, target_length=400)

for pok_list in multiple_pokemons_lists:
    # Initializing a new batch
    batch = db.batch()
    for pokemon in pok_list:
        ref = db.collection(u'pokemons').document(u'{}'.format(pokemon['number']))
        batch.set(ref, {
            u'name': u'{}'.format(pokemon['name']),
            u'types': pokemon['types'],
            u'artwork': 'https://www.pokebip.com/pokedex-images/artworks/{}.png'.format(pokemon['number']), # Artwork from pokebip
            u'icon': 'https://pokeapi.co/media/sprites/pokemon/{}.png'.format(pokemon['number']) # Icon from pokapi
        })

    batch.commit()

print(u'Done')



