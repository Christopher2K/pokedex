# ZenguDex

Zengularity Homework: A React Pokedex

## How to run locally
1. Clone this repository
2. Clone the [zengudex-api](https://gitlab.com/Christopher2K/pokedex-api) repository
3. Run `yarn install` or `npm install` in both folder
4. Create in the zengudex-api folder a `.env` file with these values:
    * `NODE_ENV=development`
    * `TWT_CONSUMER_KEY=your_own_twitter_app_consumer_key`
    * `TWT_CONSUMER_SECRET=your_own_twitter_app_secret_key`
    * `TWT_ACCESS_TOKEN_KEY=your_own_twitter_app_access_token_key`
    * `TWT_ACCESS_TOKEN_SECRET=your_own_twitter_token_secret`
5. Run `yarn start` in both folder

6. The ZenguDex should be available at [http://localhost:1234](http://localhost:1234)

This app is available online at [this url](https://zen-pokedex.firebaseapp.com)

